function changeReadStatus(message_id, read_status) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: "/admin/support/change_read_status",
        data: {
            message_id: message_id,
            read_status: read_status
        },
        success: function (response) {
            var read_badges = $('.read-messages-badge');
            var unread_badges = $('.unread-messages-badge');
            console.log(read_status);
            if (read_status) {
                read_badges.each(function () {
                    $(this).html(parseInt($(this).html())+1)
                });
                unread_badges.each(function () {
                    $(this).html(parseInt($(this).html())-1)
                });
            } else {
                read_badges.each(function () {
                    $(this).html(parseInt($(this).html())-1)
                });
                unread_badges.each(function () {
                    $(this).html(parseInt($(this).html())+1)
                });
            }
            var el = $('#message-' + message_id);
            el.fadeOut(function() {
                $(this).replaceWith(response).hide();
                $(this).fadeIn();
                bindTooltips();
            });
        }
    });
}