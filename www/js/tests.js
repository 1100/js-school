window.timer_interval = 0;
window.timeLeft = null;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function startTest(testId) {
    var questionsLeft = document.getElementById('questionsLeft');
    var testBody = document.getElementById('testBody');
    var testBegin = document.getElementById('testBegin');
    $.ajax({
        type: 'GET',
        url: '/cabinet/test/' + testId + '/start/',
        success: function (data) {
            document.testId = testId;
            testBody.innerHTML = data.question_form;
            questionsLeft.innerHTML = 'Осталось вопросов: ' + data.questions_left;
            protectForm();
            setSecondsLeft(data.time_left);
            window.timer_interval = intervalTrigger();

            testBegin.style.display = 'none';
            testBody.style.display = 'block';
        }
    });
}

function answerOrNext() {
    var is_next = Boolean(document.forms.answer_form.elements.submit.value === 'next_question');
    if (is_next) {
        getNextQuestion();
    } else {
        checkAnswer();
    }
    return false;
}

function checkAnswer() {
    var answer_form = document.forms.answer_form;
    var formData = new FormData(answer_form);
    var question_id = formData.get('question_id');
    var answer_id = formData.get('answer_id');

    $.ajax({
        type: 'GET',
        url: '/cabinet/test/' + document.testId + '/question/' + question_id + '/answer/' + answer_id,
        success: function (data) {
            console.log(data);
            if (answer_id !== data.correct_answer_id) {
                document.getElementById('answer_label_' + answer_id).classList.add('wrongAnswer');
            }
            document.getElementById('answer_label_' + data.correct_answer_id).classList.add('correctAnswer');
            console.log(data.user_points);
            document.getElementById('user_points').innerHTML = data.user_points;
            questionsLeft.innerHTML = 'Осталось вопросов: ' + data.questions_left;
            document.forms.answer_form.elements.submit.value = 'next_question';
            document.forms.answer_form.elements.submit.innerHTML = (data.questions_left > 0) ? "Следующий вопрос" : "Завершить";
            var answers = document.querySelectorAll('[name="answer_id"]');
            for (var i = 0; i < answers.length; i++) {
                answers[i].disabled = true;
            }
            document.forms.answer_form.onSubmit = getNextQuestion;
        }
    });
    return false;
}

function getNextQuestion() {
    var questionsLeft = document.getElementById('questionsLeft');
    var timeLeft = document.getElementById('timeLeft');
    var testBody = document.getElementById('testBody');
    var oneTestHeader = document.getElementById('oneTestHeader');

    $.ajax({
        type: 'GET',
        url: '/cabinet/test/' + document.testId + '/question/',
        success: function (data) {
            if(data.end_test) {
                location.reload();
            } else {
                testBody.innerHTML = data.question_form;
                protectForm();
                if (data.time_left === 0) {
                    window.clearInterval(window.timer_interval);
                } else {
                    setSecondsLeft(data.time_left);
                }
            }
        }
    });

    return false;
}

function intervalTrigger() {
    return window.setInterval( function() {
        var seconds_left = getSecondsLeft();
        if (window.timeLeft === null) {
            window.timeLeft = document.getElementById('timeLeft');
        }
        window.timeLeft.innerHTML = secondsToHHMMSS(seconds_left);
        setSecondsLeft(seconds_left-1);
        if (seconds_left <= 0 ) {
            checkAnswer();
            getNextQuestion();
            window.clearInterval(window.timer_interval);
        }
    }, 1000);
}

function setSecondsLeft(seconds_left)
{
    localStorage.setItem('seconds_left', seconds_left);
}

function getSecondsLeft()
{
    return localStorage.getItem('seconds_left');
}

function secondsToHHMMSS(seconds) {
    var hours = (Math.floor(seconds / 3600));
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    seconds = seconds - (hours * 3600) - (minutes * 60);
    if (hours < 10) {
        if (hours == 0) {
            hours = "";
        } else {
            hours = "0" + hours + ":";
        }
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return hours + minutes + ':' + seconds;
}

function protectForm() {
    var protected_block = $('#protected_block');

    protected_block.show();

    $(window).blur(function () {
        protected_block.hide();
    });
    $(window).focus(function () {
        protected_block.show();
    });

    $(window).keydown(function (e) {
        if (e.keyCode == 42 || e.keyCode == 44) {
            protected_block.hide();
        }
    });

    $(window).keyup(function (e) {
        if (e.keyCode == 42 || e.keyCode == 44) {
            protected_block.show();
        }
    });

    $(window).on('copy', function (e) {
        var clipboard = e.clipboardData;
        if (clipboard) {
            clipboard.setData('Text', '');
        }
        return false;
    });
}