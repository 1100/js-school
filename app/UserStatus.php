<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    protected $primaryKey = 'slug';
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'slug', 'name'
    ];

    public function user()
    {
        return $this->hasMany('App\User', 'status', 'slug');
    }
}
