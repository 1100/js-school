<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $primaryKey = 'slug';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'slug', 'name', 'description', 'color', 'open_rating',
    ];

    public function users()
    {
        return $this->hasMany('App\User', 'faculty');
    }

    public function admin()
    {
        return $this->belongsToMany('App\Admin', 'admin_faculty', 'faculty', 'admin_id');
    }

    public function information()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->withPivot(['required', 'display', 'rating_display'])->orderBy('position');
    }

    public function tests()
    {
        return $this->hasMany('App\Test', 'faculty', 'slug');
    }

    public function additionalInformationFields()
    {
        return $this->information->where('login', false);
    }

    public function additionalVisibleInformationFields()
    {
        return $this->information()->where([
            ['login', false],
            ['hidden', false]
        ])->get();
    }

    public function informationFieldRequired($field)
    {
        $faculty_field = $this->information->find($field->slug);
        if ($faculty_field) {
            return $faculty_field->pivot->required;
        } else {
            return false;
        }
    }

    public function informationFieldDisplay($field)
    {
        $faculty_field = $this->information->find($field->slug);
        if ($faculty_field) {
            return $faculty_field->pivot->display;
        } else {
            return false;
        }
    }

    public function informationFieldRatingDisplay($field)
    {
        $faculty_field = $this->information->find($field->slug);
        if ($faculty_field) {
            return $faculty_field->pivot->rating_display;
        } else {
            return false;
        }
    }

    public function availableAdditionalInformationFields()
    {
        $all_additional_fields = InformationField::additionalFields();
        $additional_fields = $this->additionalInformationFields();
        return $all_additional_fields->diff($additional_fields);
    }

    public function requiredInformationFields()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->wherePivot('required', true)->orderBy('position');
    }

    public function loginInformationFields()
    {
        return $this->information()->where('login', true);
    }

    public function displayInformationFields()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->wherePivot('display', true)->orderBy('position')->get();
    }

    public function displayRatingInformationFields()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->wherePivot('rating_display', true)->orderBy('position')->get();
    }

    public function addUser(array $data, $invited_key = null)
    {
        $fields = $this->information()->where('slug', '!=', 'facebook')->get();
        $user = User::create([
            'password' => (isset($data['password'])) ? bcrypt($data['password']) : null,
            'faculty' => $this->slug,
        ]);
        if ( $this->isEnabledInvite() && $invited_key) {
            $user->invited_key = $invited_key;
            $user->save();
        }
        $user_information = [];
        foreach ($fields as $field) {
            if (array_has($data, $field->slug)) {
                $user_information[$field->slug] = ['value' => $data[$field->slug]];
            }
        }
        $user->information()->attach($user_information);
        return $user;
    }

    public function invite()
    {
        return $this->hasOne('App\Invite', 'faculty', 'slug');
    }

    public function isEnabledInvite()
    {
        return !empty($this->invite);
    }

    public function addInvitePoints($user)
    {
        if ($user->status == 'approved' && !$this->invite->isMaxPoints($user)) {
            $this->invite->updateInvitePoints($user);
        }
    }

    public function getMaxInformationFieldPosition()
    {
        return $this->belongsToMany('App\InformationField', 'faculty_information_field', 'faculty', 'information_field')->withPivot(['required', 'display', 'rating_display'])->max('position');
    }

    public function getUsersData()
    {
        $information_fields = $this->information()->get();
        $users = $this->users()->with('moderation_status')->get();
        $users_data = [];
        $i = 0;
        foreach ($users as $user) {
            $users_data[$i]['Дата регистрации'] = $user->created_at->format('d.m.y H:i:s');
            $users_data[$i]['Статус'] = $user->moderation_status()->first()->name;
            foreach ($information_fields as $field) {
                $user_facebook_id = $user->getInformation($field->slug);
                if ($field->slug == 'facebook' && $user_facebook_id) {
                    $users_data[$i][$field->name] = 'https://fb.com/app_scoped_user_id/'.$user_facebook_id;
                } else {
                    $users_data[$i][$field->name] = $user->getInformation($field->slug);
                }
            }
            foreach ($this->tests()->get() as $test) {
                $test_point = $test->points()->where('user_id', $user->id)->first();
                if ($test_point) {
                    $users_data[$i][$test->name] = $test_point->number();
                } else {
                    $users_data[$i][$test->name] = null;
                }
                if ($test->bonus) {
                     $bonus_point = $test->bonus->points()->where('user_id', $user->id)->first();
                    if ($bonus_point) {
                        $users_data[$i][$test->bonus->name] = $bonus_point->number();
                    } else {
                        $users_data[$i][$test->bonus->name] = null;
                    }
                }
            }
            if ($this->isEnabledInvite()) {
                $invite_points = $this->invite->points()->where('user_id', $user->id)->first();
                if ($invite_points) {
                    $users_data[$i][$this->invite->name] = $invite_points->number();
                } else {
                    $users_data[$i][$this->invite->name] = null;
                }
            }
            $i++;
        }
        return $users_data;
    }
}