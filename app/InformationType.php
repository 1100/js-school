<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformationType extends Model
{
    protected $primaryKey = 'slug';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'slug', 'name', 'validator'
    ];

    public function informationField()
    {
        return $this->hasOne('App\InformationField', 'type', 'slug');
    }
}
