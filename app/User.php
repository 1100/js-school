<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use App\Mail\SetPassword;
use App\Mail\SendResetCode;
use App\Mail\Invite;
use App\Mail\Approved;
use App\Custom\Phone;
use App\Custom\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class User extends Authenticatable implements CanResetPasswordContract
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password', 'status', 'activation_status', 'faculty', 'rating', 'invite_key', 'invited_key',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function moderation_status()
    {
        return $this->belongsTo('App\UserStatus', 'status', 'slug');
    }

    public function activation_status()
    {
        return $this->belongsTo('App\UserActivationStatus', 'activation_status', 'id')->first();
    }

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty', 'slug');
    }

    public function information()
    {
        return $this->belongsToMany('App\InformationField', 'user_information', 'user_id', 'information_field')->withPivot('value');
    }

    public function informationValues()
    {
        return $this->hasMany('App\UserInformation');
    }

    public function avatarUrl()
    {
        if ($this->avatar) {
            if (starts_with($this->avatar, ['http://', 'https://'])) {
                return $this->avatar;
            }
            return Storage::url($this->avatar);
        } else {
            return '/images/noavatar.gif';
        }
    }

    public function supportMessages()
    {
        return $this->hasMany('App\SupportMessage', 'user_id', 'id');
    }

    public function testSessions()
    {
        return $this->hasMany('App\TestSession');
    }

    public function getDisplayName()
    {
        $first_name = $this->information()->find('first_name');
        $last_name = $this->information()->find('last_name');
        if (isset($first_name) && isset($last_name)) {
            return $first_name->pivot->value . ' ' . $last_name->pivot->value;
        } else {
            return $this->information()->where('login', true)->first()->pivot->value;
        }
    }

    public function getInformation($information_field)
    {
        $user_information = $this->information()->where('information_field', $information_field)->first();
        if ($user_information) {
            return $user_information->pivot->value;
        } else {
            return null;
        }
    }

    public function sendApprovedEmailNotification()
    {
        $email = $this->getInformation('email');
        if ($email) {
            Mail::to($email)->send(new Approved());
        }
    }

    public function sendPasswordSetEmailNotification()
    {
        $email = $this->getInformation('email');
        Mail::to($email)->send(new SetPassword($email));
    }

    public function sendPasswordResetEmailNotification($token)
    {
        Mail::to($this->getInformation('email'))->send(new ResetPassword($token));
    }

    public function sendPasswordResetSMSNotification($code) {
        $phone = Phone::getPhoneIfValid($this->getInformation('phone'))->rawNumber();
        $message = View::make('emails.sms-password-reset', ['code' => $code])->render();

        $query_data = [
            'charset' => 'utf-8',
            'login'   => env('SMSC_LOGIN'),
            'psw'     => env('SMSC_PASSWORD'),
            'phones'  => $phone,
            'mes'     => $message,
        ];
        $query = http_build_query($query_data);

        file_get_contents('http://smsc.com.ua/sys/send.php?'.$query);
    }

    public function getLoginForPasswordReset($field_name)
    {
        $field =  $this->information()->find($field_name);
        if ($field) {
            return $field->pivot->value;
        } else {
            return null;
        }
    }

    public function points()
    {
        return $this->hasMany('App\Point', 'user_id', 'id');
    }

    public function getPoints()
    {
        $ratings = [];
        foreach ($this->points as $point) {
            $ratings[] = [
                'id' => $point->id,
                'name' => $point->pointable->name,
                'points' => $point->number(),
                'type' => $point->pointable_type,
                'pointable_id' => $point->pointable->id
            ];
        }
        return $ratings;
    }

    public function generateInvite()
    {
        if (is_null($this->invite_key)) {
            $this->invite_key = md5(env('APP_KEY') . $this->id . time());
            $this->save();
        }
        return $this->invite_key;
    }

    public function sendInviteForEmail($email)
    {
        $this->generateInvite();
        Mail::to($email)->send(new Invite($this->faculty, $this->invite_key));
    }

    public static function createAndAttachFromArray($users_array)
    {
        $faculty = Faculty::find($users_array['faculty']);
        foreach ($users_array['users'] as $old_user) {
            $user = $faculty->addUser($old_user);
            $user->status = 'approved';
            $user->save();
        }
    }
}
