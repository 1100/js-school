<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Gate;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'login', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $support_messages_count;

    public function roles()
    {
        return $this->belongsToMany('App\AdminRole', 'admin_admin_role', 'admin_id', 'admin_role');
    }

    public function faculties()
    {
        return $this->belongsToMany('App\Faculty', 'admin_faculty', 'admin_id', 'faculty');
    }

    public function readSupportMessages()
    {
        return $this->hasMany('App\SupportMessage', 'admin_id', 'id');
    }

    public function getAllSupportMessagesCount()
    {
        if (is_null($this->support_messages_count)) {
            $this->supportMessagesCount();
        }
        return $this->support_messages_count['all'];
    }

    public function getUnreadSupportMessagesCount()
    {
        if (is_null($this->support_messages_count)) {
            $this->supportMessagesCount();
        }
        return $this->support_messages_count['unread'];
    }

    public function getReadSupportMessagesCount()
    {
        if (is_null($this->support_messages_count)) {
            $this->supportMessagesCount();
        }
        return $this->support_messages_count['read'];
    }

    protected function supportMessagesCount()
    {
        if (Gate::allows('role', 'admin')) {
            $this->support_messages_count['all'] = SupportMessage::count();
            $this->support_messages_count['unread'] = SupportMessage::where('read_status', false)->count();
            $this->support_messages_count['read'] = SupportMessage::where('read_status', true)->count();
        } else if (Gate::allows('role', 'curator')) {
            $faculties = $this->faculties()->pluck('slug')->toArray();
            $this->support_messages_count['all'] = SupportMessage::whereIn('faculty', $faculties)->count();
            $this->support_messages_count['unread'] = SupportMessage::whereIn('faculty', $faculties)->where('read_status', false)->count();
            $this->support_messages_count['read'] = SupportMessage::whereIn('faculty', $faculties)->where('read_status', true)->count();
        } else {
            $this->support_messages_count['all'] = 0;
            $this->support_messages_count['unread'] = 0;
            $this->support_messages_count['read'] = 0;
        }
    }
}
