<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'points', 'invites_count', 'faculty',
    ];

    public function points()
    {
        return $this->morphMany('App\Point', 'pointable');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty', 'slug');
    }

    public function getInvitePointPercent()
    {
        if ($this->invites_count != 0) {
            return (int)round((100 / $this->invites_count));
        } else {
            return 0;
        }
    }

    public function isMaxPoints($user)
    {
        $user_points = $this->points()->where('user_id', $user->id)->first();
        if($user_points && $user_points->percent < 100) {
            return true;
        } else {
            return false;
        }
    }

    public function updateInvitePoints($user)
    {
        $user_point = $this->points()->where('user_id', $user->id)->first();
        if (is_null($user_point)) {
            $user_point = Point::create([
                'user_id' => $user->id,
                'percent' => 0,
                'pointable_id' => $this->id,
                'pointable_type' => get_class($this),
            ]);
        }
        $user_point->addPercent($this->getInvitePointPercent());
        $user_point->save();
    }
}
