<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'user_id', 'pointable_id', 'pointable_type', 'percent',
    ];

    public function pointable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function number()
    {
        return (int)round(($this->pointable->points * $this->percent)/100);
    }

    public function addPercent($percent)
    {
        $this->percent += $percent;
        if($this->percent > 100) {
            $this->percent = 100;
        }
        $this->save();
        $rating = 0;
        foreach (Point::where('user_id', $this->user->id)->get() as $point) {
            $rating += $point->number();
        }
        $this->user->rating = $rating;
        $this->user->save();
    }

    public function updateUsersRatings()
    {
        $rating = 0;
        foreach (Point::where('user_id', $this->user->id)->get() as $point) {
            $rating += $point->number();
        }
        $this->user->rating = $rating;
        $this->user->save();
    }
}
