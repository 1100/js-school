<?php

namespace App\Custom\Contracts\Auth;

interface CanResetPassword
{

    /**
     * Get the login where password reset email or sms code are sent.
     *
     * @param string $type login field
     * @return string
     */
    public function getLoginForPasswordReset($type);

    /**
     * Send the password reset email notification.
     *
     * @param integer $token
     * @return void
     */
    public function sendPasswordResetEmailNotification($token);

    /**
     * Send the password reset sms notification.
     *
     * @param integer $code
     * @return void
     */
    public function sendPasswordResetSMSNotification($code);
}