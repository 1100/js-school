<?php

namespace App\Custom;

use Exception;
use Throwable;

class InvalidPhoneException extends Exception
{
    public function __construct($message = "Invalid phone number", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}