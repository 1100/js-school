<?php

namespace App\Custom\Facades;

use Illuminate\Support\Facades\Password as IlluminatePasswordFacade;

class Password extends IlluminatePasswordFacade
{
    /**
     * Constant representing a successfully sent sms with code.
     *
     * @var string
     */
    const RESET_SMS_SENT = 'passwords.sentSMS';
}