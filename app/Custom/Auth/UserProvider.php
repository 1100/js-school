<?php

namespace App\Custom\Auth;

use App\UserInformation;
use App\User;
use Illuminate\Auth\EloquentUserProvider;

class UserProvider extends EloquentUserProvider
{

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \App\Custom\Contracts\Auth\CanResetPassword|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return null;
        }
        $credentials = array_except($credentials, ['password', 'password_confirmation']);

        $query = UserInformation::query();

        foreach ($credentials as $key => $value) {
            $query->orWhere([
                ['information_field', $key],
                ['value', $value],
            ]);
        }

//        TODO: Refactor like where(UserInformation.some.user_id, user_id)...

        $user_id = $query->pluck('user_id')->mode()[0];
        $user_ids = $query->pluck('user_id')->toArray();
        $ids_count = 0;
        foreach ($user_ids as $id) {
            if ($user_id == $id) {
                $ids_count++;
            }
        }

        if (count($credentials) == $ids_count) {
            return User::find($user_id);
        } else {
            return null;
        }
    }

}
