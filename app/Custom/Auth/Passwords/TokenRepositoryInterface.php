<?php

namespace App\Custom\Auth\Passwords;

use App\Custom\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

interface TokenRepositoryInterface
{
    /**
     * Create a new token.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword $user
     * @param string $login_field
     * @return string
     */
    public function create(CanResetPasswordContract $user, $login_field);

    /**
     * Determine if a token record exists and is valid.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword  $user
     * @param  array  $secret
     * @return bool
     */
    public function exists(CanResetPasswordContract $user, $secret);

    /**
     * Delete a token record.
     *
     * @param  \App\Custom\Contracts\Auth\CanResetPassword  $user
     * @return void
     */
    public function delete(CanResetPasswordContract $user);

    /**
     * Delete expired tokens.
     *
     * @return void
     */
    public function deleteExpired();
}