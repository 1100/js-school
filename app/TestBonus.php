<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestBonus extends Model
{
    public $timestamps = false;
    protected $table = 'tests_bonus';

    protected $fillable = [
        'name', 'test_id', 'points', 'bonus_count'
    ];

    public function points()
    {
        return $this->morphMany('App\Point', 'pointable');
    }

    public function test()
    {
        return $this->belongsTo('App\Test', 'test_id', 'id');
    }
}
