<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculties = Faculty::orderBy('slug', 'desc')->get();
        $posts = Post::orderBy('created_at', 'desc');
        return view('home', ['faculties' => $faculties, 'posts' => $posts->paginate(3)]);
    }
}
