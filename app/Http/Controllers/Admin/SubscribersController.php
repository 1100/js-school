<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Support\Facades\Auth;
use Gate;

class SubscribersController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }

    public function show()
    {
        if (Gate::allows('permission', 'subscribers')) {
            $subscribers = Subscriber::paginate(15);
            return view('admin.subscribers', [
                'subscribers' => $subscribers,
            ]);
        } else {
            return abort(404);
        }
    }
}