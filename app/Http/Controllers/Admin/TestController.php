<?php

namespace App\Http\Controllers\Admin;

use App\Faculty;
use App\Test;
use App\Question;
use App\Answer;
use App\TestBonus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gate;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }

    public function showAll($faculty)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $tests = Test::where('faculty', $faculty->slug)->get();
            return view('admin.faculty.test.all', [
                'faculty' => $faculty,
                'tests' => $tests,
            ]);
        } else {
            return abort(404);
        }
    }

    public function showAddTestForm($faculty)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.test.add', [
                'faculty' => $faculty,
            ]);
        } else {
            return abort(404);
        }
    }

    public function addTest(Request $request, $faculty)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $this->validate($request, [
                'name' => 'string|required',
                'description' => 'string|nullable',
                'points' => 'numeric|min:0|required',
                'question_count' => 'numeric|min:1|required',
                'duration' => 'numeric|min:1|required',
                'start_date' => 'date|required|after_or_equal:today',
                'end_date' => 'date|required|after:start_date',
                'bonus_points' => 'integer|min:0',
                'bonus_count' => 'integer|min:0'
            ]);
            $test = Test::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'faculty' => $faculty->slug,
                'points' => $request->input('points'),
                'question_count' => $request->input('question_count'),
                'duration' => $request->input('duration'),
                'start_date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('start_date')),
                'end_date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('end_date')),
            ]);
            if ($request->has('bonus') && $request->input('bonus') == 'on') {
                $test->updateBonus($request->input('bonus_points'), $request->input('bonus_count'));
            }
            return redirect()->route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function showEditForm($faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            return view('admin.faculty.test.edit', [
                'faculty' => $faculty,
                'test' => $test,
            ]);
        } else {
            return abort(404);
        }
    }

    public function edit(Request $request, $faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $this->validate($request, [
                'name' => 'string',
                'description' => 'string|nullable',
                'points' => 'numeric|min:0|required',
                'question_count' => 'numeric|min:1|max:'.$test->questions->count().'|required',
                'duration' => 'numeric|min:1|required',
                'start_date' => 'date|required',
                'end_date' => 'date|required|after:start_date',
                'bonus_points' => 'integer|min:0',
                'bonus_count' => 'integer|min:0'
            ]);
            $test->fill([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'faculty' => $faculty->slug,
                'points' => $request->input('points'),
                'question_count' => $request->input('question_count'),
                'duration' => $request->input('duration'),
                'start_date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('start_date')),
                'end_date' => Carbon::createFromFormat('Y-m-d H:i:s', $request->input('end_date')),
            ]);
            $test->save();
            if ($request->has('bonus') && $request->input('bonus') == 'on') {
                $test->updateBonus($request->input('bonus_points'), $request->input('bonus_count'));
            } else {
                $test->removeBonus();
            }
            $test->updateUsersRatings();
            return redirect()->route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]);
        } else {
            return abort(404);
        }
    }

    public function showAllQuestions($faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $questions = Question::where('test_id', $test_id)->paginate(10);
            if ($test) {
                return view('admin.faculty.test.all_questions', [
                    'faculty' => $faculty,
                    'test' => $test,
                    'questions' => $questions,
                ]);
            }
        }
        return abort(404);
    }

    public function showAddQuestionForm($faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            return view('admin.faculty.test.edit_question_form', [
                'faculty' => $faculty,
                'test' => $test,
            ]);
        }
        return abort(404);
    }

    public function addQuestion(Request $request, $faculty, $test_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $this->validate($request, [
                'name' => 'string|required',
            ]);
            $question = Question::create([
                'name' => $request->input('name'),
                'test_id' => $test->id,
            ]);
            foreach ($request->input('new_answers') as $new_answer) {
                if (isset($new_answer['name']) && !empty($new_answer['name'])) {
                    Answer::create([
                        'name' => $new_answer['name'],
                        'is_correct' => (isset($new_answer['is_correct']) && $new_answer['is_correct'] == 'on') ? true : false,
                        'question_id' => $question->id,
                    ]);
                }
            }
            return redirect()->route('admin.faculties.faculty.test.question.all', ['faculty' => $faculty->slug, 'test_id' => $test->id]);
        }
        return abort(404);
    }

    public function showEditQuestionForm($faculty, $test_id, $question_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $question = Question::where('id', $question_id)->with('answers')->first();
            if ($question) {
                return view('admin.faculty.test.edit_question_form', [
                    'faculty' => $faculty,
                    'test' => $test,
                    'question' => $question,
                ]);
            }
        }
        return abort(404);
    }

    public function editQuestion(Request $request, $faculty, $test_id, $question_id)
    {
        if (Gate::allows('permission', 'tasks') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($test_id);
            $question = Question::findOrFail($question_id);
            $this->validate($request, [
                'name' => 'string|required',
            ]);
            $question->name = $request->input('name');
            $question->save();
            if ($request->has('answers')) {
                foreach ($request->input('answers') as $key =>  $answer) {
                    if (!empty($answer['name'])) {
                        $current_answer = Answer::find($key);
                        $current_answer->name = $answer['name'];
                        $current_answer->is_correct = (isset($answer['is_correct']) && $answer['is_correct'] == 'on') ? true : false;
                        $current_answer->save();
                    }
                }
            }
            if ($request->has('new_answers')) {
                foreach ($request->input('new_answers') as $new_answer) {
                    if (isset($new_answer['name']) && !empty($new_answer['name'])) {
                        Answer::create([
                            'name' => $new_answer['name'],
                            'is_correct' => (isset($new_answer['is_correct']) && $new_answer['is_correct'] == 'on') ? true : false,
                            'question_id' => $question->id,
                        ]);
                    }
                }
            }
            return redirect()->route('admin.faculties.faculty.test.question.all', ['faculty' => $faculty->slug, 'test_id' => $test->id]);
        }
        return abort(404);
    }
}
