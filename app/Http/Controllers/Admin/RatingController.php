<?php

namespace App\Http\Controllers\Admin;

use App\Faculty;
use App\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gate;

class RatingController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        Auth::shouldUse('admin');
    }

    public function show($faculty)
    {
        if (Gate::allows('permission', 'rating') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            return view('admin.faculty.rating.index', [
                'faculty' => $faculty,
                'users' => $faculty->users()->where('status', 'approved')->orderBy('rating', 'desc')->paginate(15),
            ]);
        } else {
            return abort(404);
        }
    }

    public function showUser($faculty, $user_id)
    {
        if (Gate::allows('permission', 'rating') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $user = $faculty->users()->find($user_id);
            $user_ratings = $user->getPoints();
            return view('admin.faculty.rating.user', [
                'faculty' => $faculty,
                'user' => $user,
                'user_ratings' => $user_ratings,
            ]);
        } else {
            return abort(404);
        }
    }

    public function resetUserTestSession(Request $request, $faculty, $user_id)
    {
        if (Gate::allows('permission', 'reset_test') && Gate::allows('faculty', $faculty)) {
            $faculty = Faculty::findOrFail($faculty);
            $test = Test::findOrFail($request->input('test_id'));
            $test->resetUserSession($user_id);
            return back();
        } else {
            return abort(404);
        }
    }
}
