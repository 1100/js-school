<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Custom\Phone;
use App\SupportMessage;

class ContactsController extends Controller
{
    public function index()
    {
        return view('contacts');
    }

    public function send(Request $request)
    {
        Validator::make($request->all(), [
            'email' => 'email|nullable|required_without:phone',
            'phone' => 'phone|nullable|required_without:email',
            'first_name' => 'string|required',
            'last_name' => 'string|nullable',
            'message' => 'required'
        ])->validate();


        $phone = Phone::getPhoneIfValid($request->input('phone'));

        SupportMessage::create([
            'email' => $request->input('email'),
            'phone' => (!is_null($phone)) ? $phone->formattedNumber() : null,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'message' => $request->input('message'),
        ]);
        return $this->contactSuccessResponse(__('contact.send.success'));
    }

    protected function contactSuccessResponse($response)
    {
        return back()->with('status', $response);
    }
}
