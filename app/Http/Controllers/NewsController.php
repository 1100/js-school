<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $posts = Post::where('faculty', $user->faculty)->orderBy('created_at', 'desc');
        $faculties = Faculty::orderBy('slug', 'desc')->get();
        return view('posts', [
            'faculties' => $faculties,
            'posts' => $posts->paginate(3)
        ]);
    }

    public function showPost($post_id)
    {
        $faculties = Faculty::orderBy('slug', 'desc')->get();
        $post = Post::findorFail($post_id);
        return view('post', [
            'faculties' => $faculties,
            'post' => $post
        ]);
    }
}
