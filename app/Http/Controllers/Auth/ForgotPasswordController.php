<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Custom\Phone;
use App\Custom\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendReset(Request $request)
    {
        $this->validate($request, ['login' => 'required']);

        if ($phone = Phone::getPhoneIfValid($request->input('login'))) {
            $response = $this->broker()->sendResetSMS([
                'phone' => $phone->formattedNumber()
            ]);
        } else {
            $credentials['email'] = $request->input('login');
            $response = $this->broker()->sendResetLink([
                'email' => $request->input('login')
            ]);
        }

        if ($response == Password::RESET_LINK_SENT) {
            return $this->sendResetLinkResponse($response);
        } elseif ($response == Password::RESET_SMS_SENT) {
            return redirect()->route('password.reset_by_sms.form', [
                'token' => $this->broker()->getCurrentToken(),
                'phone' => $this->broker()->getCurrentUser()->getLoginForPasswordReset('phone'),
            ]);
        } else {
            return $this->sendResetLinkFailedResponse($request, $response);
        }
    }

    public function showLinkRequestForm($email = null)
    {
        return view('auth.passwords.request', ['email' => $email]);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return back()->withErrors(
            ['login' => trans($response)]
        );
    }
}
