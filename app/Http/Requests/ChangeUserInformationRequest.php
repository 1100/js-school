<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ChangeUserInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        $fields = $user->faculty()->requiredInformationFields()->where('slug', '!=', 'facebook')->get();
        $rules = [];
        foreach ($fields as $field) {
            $rules[$field->slug] = [
                'required',
                $field->information_type->validator,
            ];
            if ($field->login) {
                if ($user) {
                    $rules[$field->slug][] = 'uniqueLogin:' . $user->getInformation($field->slug);
                } else {
                    $rules[$field->slug][] = 'uniqueLogin';
                }
            }
        }
        return $rules;
    }
}
