$(document).ready(function(){

    var url = "/subscribe";

    $("#btn-subscribe").click(function (e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                email: $('#subscribe-email').val()
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('#subscribe-success-message').text(response.message);
                    $('#subscribe-success').slideDown();
                }
            },
            error: function (response) {
                $('#subscribe-error-message').text(response.message);
                $('#subscribe-error').slideDown();
            }
        });
    });
});