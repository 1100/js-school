@extends('layouts.default')

@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
                <h1>Помилка 404. Сторінка не знайдена</h1>
                Запрошеної Вами сторінки не існує.

        </div>
    </div>
@endsection