@extends('layouts.with_sidebar')
@section('slider')
@endsection
@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <ul class="blog clearfix">
                    @foreach($posts as $post)
                    <li class="post">
                        <ul class="comment_box">
                            <li class="date clearfix animated_element animation-slideRight">
                                <div class="value">{{ $post->created_at->format('d.m.y') }}</div>
                                <div class="arrow_date"></div>
                            </li>
                        </ul>
                        <div class="post_content">
                            @if($post->image)
                                <a class="post_image" href="{{ route('post', ['post_id' => $post->id]) }}" title="{{ $post->title }}">
                                    <img src="{{ Storage::url($post->image) }}" alt="{{ $post->title }}" />
                                </a>
                            @endif
                            <h2>
                                <a href="{{ route('post', ['post_id' => $post->id]) }}" title="{{ $post->title }}">
                                    {{ $post->title }}
                                </a>
                            </h2>
                                {!! $post->content !!}
                            <a title="Відкрити" href="{{ route('post', ['post_id' => $post->id]) }}" class="more">
                                Відкрити &rarr;
                            </a>
                        </div>
                    </li>
                    @endforeach
                </ul>
                <div class="page_margin_top">
                {{ $posts->links() }}
                </div>
            </div>
@endsection

@section('sidebar')
    @include('layouts.partials.sidebar')
        </div>
    </div>
@endsection