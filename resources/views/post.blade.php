@extends('layouts.with_sidebar')
@section('slider')
@endsection
@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <div class="post">
                    @if($post->image)
                    <a class="post_image" href="{{ route('post', ['post_id' => $post->id]) }}" title="{{ $post->title }}">
                        <img src="{{ Storage::url($post->image) }}" alt="{{ $post->title }}" />
                    </a>
                    @endif
                    <h1>{{ $post->title }}</h1>
                    {!! $post->content !!}
                </div>
            </div>
@endsection

@section('sidebar')
    @include('layouts.partials.sidebar')
        </div>
    </div>
@endsection