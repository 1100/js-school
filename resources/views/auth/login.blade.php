@extends('layouts.default')

@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
            <h3 class="box_header">
                Вхід
            </h3>
            <div class="columns clearfix">
            <div class="page_left">
                <div class="columns clearfix">
                    <form class="form" role="form" method="POST">
                        {{ csrf_field() }}

                        @if ($fields->count() >= 1)
                            <label for="login">Логін</label>
                            <div class="block{{ $errors->has('login') ? ' has-error' : '' }}">
                                <input id="login" class="text_input" name="login" type="text" required value="{{ old('login') }}"/>
                                @if ($errors->has('login'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('login') }}</strong>
                                </span>
                                @endif
                            </div>
                        @endif

                        <label for="password">Пароль</label>
                        <div class="block{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" class="text_input" name="password" type="password" required />
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="block">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Запам'ятати мене
                            </label>
                            <a href="{{ route('password.request') }}">
                                Забули пароль?
                            </a>
                        </div>

                        <button type="submit" class="more blue">Війти</button>
                    </form>

                    @if ($fields->find('facebook'))
                        <p>Для участников с привязанным facebook-аккаунтом:</p>
                        @include ('user_information.input.'.$fields->find('facebook')->type)
                        @php $fields->forget('facebook') @endphp
                    @endif
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
