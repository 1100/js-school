@extends('layouts.default')

@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
            <h3 class="box_header">
                Група "{{ $faculty->name }}"
            </h3>
            <div class="columns clearfix">
                <p>
                    Для регистрации пожалуйста заполните форму:
                </p>
            </div>
            <div class="page_left">
                <h3 class="box_header">
                    Реєстрація в групі "{{ $faculty->name }}"
                </h3>
                <div class="columns clearfix">
                    <form class="form" role="form" method="POST" action="{{ route('register', ['faculty' => $faculty->slug]) }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="faculty" value="{{ $faculty->slug }}">
                        @isset( $invited_key )
                            <input type="hidden" name="invited_key" value="{{ $invited_key }}">
                        @endisset

                        @if ($fields->find('facebook'))
                            @include ('user_information.input.'.$fields->find('facebook')->type)
                        @endif

                        @if($errors->any())
                            <div id="register-errors" class="alert alert-danger alert-dismissable error">
                                <span class="close" data-hide="register-errors" aria-label="close">&times;</span>
                                <b>Помилка</b>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{!! $error !!}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @foreach ($fields as $field)
                            @if ($field->slug != 'facebook')
                                @include ('user_information.input.'.$field->type)
                            @endif
                        @endforeach

                        <label for="password">Пароль</label>
                        <div class="block{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" class="text_input" name="password" type="password" required />
                        </div>

                        <label for="password-confirm">Повторіть пароль</label>
                        <div class="block">
                            <input id="password-confirm" class="text_input" name="password_confirmation" type="password" required />
                        </div>

                        <button type="submit" class="more blue">Зареєструватись</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
