@extends('layouts.default')

@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
            <h3 class="box_header">
                Відновлення паролю
            </h3>
            <div class="columns clearfix">
                <div class="page_left">
                    <div class="columns clearfix">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }} <br>
                                Если Вы не получили email-сообщение со ссылкой для сброса пароля, обратитесь в <a href="{{ route('contacts') }}">поддержку</a>.
                            </div>
                        @endif
                        <form class="form" role="form" method="POST" action="{{ route('password.send') }}">
                            {{ csrf_field() }}


                                <label for="login">E-Mail</label>
                                <div class="block{{ $errors->has('login') ? ' has-error' : '' }}">
                                    <input id="login" class="text_input" name="login" type="text" required value="{{ (old('login')) ? old('login') : ((!is_null($email)) ? $email : '') }}"/>
                                    @if ($errors->has('login'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('login') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            <button type="submit" class="more blue">Відновити пароль</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
