@extends('layouts.default')

@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
            <h3 class="box_header">
                Відновлення паролю
            </h3>
            <div class="columns clearfix">
                <div class="page_left">
                    <div class="columns clearfix">
                        <form class="form" role="form" method="POST" action="{{ route('password.request') }}">
                            {{ csrf_field() }}

                            <input type="hidden" name="token" value="{{ $token }}">
                            @isset( $phone )
                                <input type="hidden" name="phone" value="{{ $phone }}">
                            @endisset

                            <label for="email">E-mail</label>
                            <div class="block{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" class="text_input" type="email" name="email" value="{{ $email or old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label for="login">Новий пароль</label>
                            <div class="block{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" class="text_input" name="password" type="password" required />
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <label for="password_confirmation">Повторіть пароль</label>
                            <div class="block{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <input id="password_confirmation" class="text_input" name="password_confirmation" type="password" required />
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <button type="submit" class="more blue">Відновити пароль</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
