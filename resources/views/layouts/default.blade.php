<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>Школа JS</title>
    <!--meta-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="keywords" content="Geberit, Geberit Academy, Академия Сантехнических Наук" />
    <meta name="description" content="Академия Сантехнических Наук" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <!--style-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/reset.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/superfish.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fancybox/jquery.fancybox.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.qtip.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui-1.9.2.custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/colors.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animations.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/tests.css') }}" />
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
    <!--js-->
    <script type="text/javascript" src="{{ asset('js/jquery-1.12.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-migrate-1.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.ba-bbq.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-ui-1.9.2.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.carouFredSel-5.6.4-packed.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.sliderControl.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.timeago.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.hint.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.isotope.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.isotope.masonry.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.fancybox-1.3.4.pack.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.qtip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.blockUI.js') }}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key= AIzaSyA2W_dEztMkbz-FHIIyb-V13ETccsfy3mA"></script>
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div class="site_container">
        @include('layouts.partials.header')

        @yield('slider')

        @yield('content')

        @yield('sidebar')

        @include('layouts.partials.footer')
    </div>
    <script type="text/javascript">
        // remove '#_=_' hash after facebook
        if (window.location.hash && window.location.hash === '#_=_') {
            if (window.history && history.pushState) {
                window.history.pushState("", document.title, window.location.pathname);
            } else {
                // prevent scrolling by storing the page's current scroll offset
                var scroll = {
                    top: document.body.scrollTop,
                    left: document.body.scrollLeft
                };
                window.location.hash = '';
                // restore the scroll offset, should be flicker free
                document.body.scrollTop = scroll.top;
                document.body.scrollLeft = scroll.left;
            }
        }
    </script>
</body>
</html>
