<div class="header_container">
    <div class="header clearfix">
        <div class="header_left">
            <h1>Школа JS</h1>
        </div>

        <div class="mobile_menu">
            <div class="hamburger hamburger--spring">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header_menu_container">
    <div class="header_menu">
        <ul class="sf-menu">
            <li class="submenu {!! classActivePath('/', false, 'selected') !!}">
                <a href="/" title="Головна">
                    Головна
                </a>
            </li>
            @if (Auth::user())
            <li class="submenu {!! classActivePath('news', true, 'selected') !!}">
                <a href="{{ route('news') }}" title="Новини курсу">
                    Новини курсу
                </a>
            </li>
            @endif
            <li class="submenu {!! classActivePath('cabinet', true, 'selected') !!}">
                <a href="{{ route('cabinet.dashboard') }}" title="Кабінет">
                    Кабінет
                </a>
            </li>
            <li class="submenu {!! classActivePath('contacts', true, 'selected') !!}">
                <a href="{{ route('contacts') }}" title="Контакти">
                    Контакти
                </a>
            </li>
        @if (Auth::guard('admin')->user())
                <li class="submenu">
                    <a href="{{ route('admin.faculties.show') }}" title="Контакты">
                        Адмін-панель
                    </a>
                </li>
        @endif
        @if (Auth::user())
            <li class="submenu submenu_right logout_icon">
                <a href="#" title="Вийти" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Вийти
                </a>
            </li>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @else
            <li class="submenu submenu_right login_icon">
                <a href="{{ route('login') }}" title="Вхід">
                    Вхід
                </a>
            </li>
        @endif
        </ul>
    </div>
</div>