<div class="page_right">
    <div class="sidebar_box first">
        <h3 class="box_header">
            Інформація
        </h3>
        <ul class="sidebar_menu">
            <li>
                <div class="sidebar_menu_content clearfix">
                    <div class="item_content clearfix">
                        <a class="thumb_image" href="#" title="Primary Health Care">
                            <img src="/images/icon_info_75.png" alt="" />
                        </a>
                        <p>
                            Для отримання повного доступу до матеріалів Школи та участі в тестуванні необхідно зареєструватись
                        </p>
                    </div>
                    <div class="item_footer clearfix">
                        <a class="more blue icon_small_arrow margin_right_white" href="{{ route('login') }}" title="Вхід для студентів">Вхід для студентів</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>