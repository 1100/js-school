<div class="page_right">
    <div class="sidebar_box first">
        <div class="item_content clearfix">
            <div class="avatar_wrapper" style="background-image: url('{{ $user->avatarUrl() }}')">
            </div>
            <div class="text profile">
                <p>
                    <b>{{ $user->getDisplayName() }}</b>
                    <br>
                    Бали: <b id="user_points">{{ $user->rating }}</b>
                </p>
            </div>
        </div>
    </div>
    <div class="sidebar_box first">
        <ul class="sidebar_menu">
            <a href="{{ route('cabinet.dashboard') }}">
                <li class="sidebar_menu_item{!! classActivePath('cabinet') !!}">
                    Огляд
                </li>
            </a>
            <a href="{{ route('cabinet.tests') }}">
                <li class="sidebar_menu_item{!! classActivePath('cabinet.tests') !!}">
                    Тестування
                </li>
            </a>
            @if ($user->faculty()->first()->open_rating)
            <a href="{{ route('cabinet.ratings') }}">
                <li class="sidebar_menu_item{!! classActivePath('cabinet.ratings') !!}">
                    Рейтинги
                </li>
            </a>
            @endif
            <a href="{{ route('cabinet.information.show') }}">
                <li class="sidebar_menu_item{!! classActivePath('cabinet.information') !!}">
                    Моя інформація
                </li>
            </a>
            <a href="{{ route('cabinet.help') }}">
                <li class="sidebar_menu_item{!! classActivePath('cabinet.help') !!}">
                    Допомога
                </li>
            </a>
        </ul>
    </div>
</div>