<div class="footer_container">
    <div class="footer">
        <div class="copyright_area clearfix">
            <div class="copyright_left">
                © Copyright - <a href="#" title="JS-School" target="_blank">JS-School</a>
            </div>
            <div class="copyright_right">
                <a class="scroll_top icon_small_arrow top_white" href="#top" title="На початок сторінки">Наверх</a>
            </div>
        </div>
    </div>
</div>