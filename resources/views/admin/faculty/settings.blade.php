@extends('admin.layouts.faculty')
@section('faculty_content')
<div class="row">
    <form class="{{ $errors->has('fields') ? ' has-error' : '' }} col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.update_display_fields', ['faculty' => $faculty->slug]) }}">
        {{ csrf_field() }}
        <a name="display_user_fields"></a>
        <h4>Відображувана інформація студентів в таблицях</h4>
        <div class="form-group">
            <table class="admin_table">
                <thead>
                    <tr>
                        <th>Поле</th>
                        <th>в загальній</th>
                        <th>в рейтингу</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($faculty->information as $field)
                    <tr>
                        <td>{{ $field->name }}</td>
                        <td>
                            <input type="checkbox" name="display_fields[{{ $field->slug }}]" @if($faculty->informationFieldDisplay($field)) checked @endif value="{{ $field->slug }}">
                        </td>
                        <td>
                            <input type="checkbox" name="rating_display_fields[{{ $field->slug }}]" @if($faculty->informationFieldRatingDisplay($field)) checked @endif value="{{ $field->slug }}">
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <label for="open-rating">
                <input id="open-rating" type="checkbox" name="open_rating" @if($faculty->open_rating == true) checked @endif>
                Показувати таблицю рейтингу студентам в особистому кабінеті
            </label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Зберегти</button>
        </div>
    </form>

    <h4>Запросити друга</h4>

@if ( $can_invite )
    @if ( $faculty->isEnabledInvite() )

        <div class="alert alert-warning" role="alert">
            Увага! Вимкнення приведе до видалення всіх запрошень<br>
            <a href="#" class="btn btn-danger"
               onclick="event.preventDefault();
               document.getElementById('disable-invite-form').submit();">Вимкнути</a>
            <form id="disable-invite-form" style="display: none;" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.disable_invite', ['faculty' => $faculty->slug]) }}">
                {{ csrf_field() }}
            </form>
        </div>

        <form class="{{ $errors->has('invite') ? ' has-error' : '' }} col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.update_invite', ['faculty' => $faculty->slug]) }}">
            {{ csrf_field() }}

            <div class="form-group">
            <label for="name">Назва</label>
            <div class="block{{ $errors->has('invite_name') ? ' has-error' : '' }}">
                <input id="invite_name" class="form-control" name="invite_name" type="text" value="{{ (old('invite_name') ? old('invite_name') : $faculty->invite->name) }}"/>
            @if ($errors->has('invite_name'))
                <span class="help-block">
                  <strong>{{ $errors->first('invite_name') }}</strong>
                </span>
            @endif
            </div>
            </div>

            <div class="form-group">
            <label for="name">Максимум балів (за всі запрошения)</label>
            <div class="block{{ $errors->has('invite_points') ? ' has-error' : '' }}">
                <input id="invite_points" class="form-control" name="invite_points" type="number" value="{{ (old('invite_points') ? old('invite_points') : $faculty->invite->points) }}"/>
                @if ($errors->has('invite_points'))
                    <span class="help-block">
                  <strong>{{ $errors->first('invite_points') }}</strong>
                </span>
                @endif
            </div>
            </div>

            <div class="form-group">
            <label for="name">Кількість винагороджуваних запрошень з одного аккаунту</label>
            <div class="block{{ $errors->has('invites_count') ? ' has-error' : '' }}">
                <input id="invites_count" class="form-control" name="invites_count" type="number" value="{{ (old('invites_count') ? old('invites_count') : $faculty->invite->invites_count) }}"/>
                @if ($errors->has('invites_count'))
                    <span class="help-block">
                  <strong>{{ $errors->first('invites_count') }}</strong>
                </span>
                @endif
            </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Зберегти</button>
            </div>
        </form>

    @else

        <form class="form-inline col-md-12" role="form" method="POST" action="{{ route('admin.faculties.faculty.settings.enable_invite', ['faculty' => $faculty->slug]) }}">
            {{ csrf_field() }}
            <div class="form-group">
                <button type="submit" class="btn btn-success">Увімкнути</button>
            </div>
        </form>

    @endif
@else
    <div class="alert alert-warning" role="alert">
        Потрібно логін-поле email, для того, щоб можна було увімкнути<br>
        <a href="{{ route('admin.faculties.faculty.user_information', ['faculty' => $faculty->slug]) }}">
            Управління полями
        </a>
    </div>
@endif
</div>
@endsection