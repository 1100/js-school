@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
                <i class="fa fa-arrow-left"></i>
                Всі тести
            </a>
            <h4 class="pull-left">Новий тест</h4>
        </div>
        <form role="form" method="POST" action="{{ route('admin.faculties.faculty.test.add', ['faculty' => $faculty->slug]) }}" class="col-md-12">
            {{ csrf_field() }}

            @include('admin.faculty.test.edit_test_inputs')

            <div class="form-group">
            <button type="submit" class="btn btn-primary">Додати</button>
            </div>
        </form>
    </div>
@endsection