<div class="form-group">
<label for="name">Назва</label>
<div class="block{{ $errors->has('name') ? ' has-error' : '' }}">
    <input id="name" class="form-control" name="name" type="text" value="{{ (old('name') ? old('name') : ((isset($test)) ? $test->name : '' )) }}"/>
    @if ($errors->has('name'))
        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
<label for="name">Опис</label>
<div class="block{{ $errors->has('description') ? ' has-error' : '' }}">
    <input id="description" class="form-control" name="description" type="text" value="{{ (old('description') ? old('description') : ((isset($test)) ? $test->description : '' )) }}"/>
    @if ($errors->has('description'))
        <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
<label for="points">Бали за повний тест</label>
<div class="block{{ $errors->has('points') ? ' has-error' : '' }}">
    <input id="points" class="form-control" name="points" type="number" value="{{ (old('points') ? old('points') : ((isset($test)) ? $test->points : '0' )) }}"/>
    @if ($errors->has('points'))
        <span class="help-block">
            <strong>{{ $errors->first('points') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
<label for="duration">Тривалість тесту в хвилинах</label>
<div class="block{{ $errors->has('duration') ? ' has-error' : '' }}">
    <input id="points" class="form-control" name="duration" type="number" value="{{ (old('duration') ? old('duration') : ((isset($test)) ? $test->duration : '1' )) }}"/>
    @if ($errors->has('duration'))
        <span class="help-block">
            <strong>{{ $errors->first('duration') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
<label for="question_count">Кількість запитань при проходженні</label>
<div class="block{{ $errors->has('question_count') ? ' has-error' : '' }}">
    <input id="question_count" class="form-control" name="question_count" type="number" value="{{ (old('question_count') ? old('question_count') : ((isset($test)) ? $test->questions_count() : '1' )) }}"/>
    @if ($errors->has('question_count'))
        <span class="help-block">
            <strong>{{ $errors->first('question_count') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
<label for="start_date">Час початку тестування</label>
<div class="block{{ $errors->has('start_date') ? ' has-error' : '' }}">
    <input id="start_date" class="datetimepicker form-control" name="start_date" type="text" value="{{ (old('start_date') ? old('start_date') : ((isset($test)) ? $test->start_date : '' )) }}"/>
    @if ($errors->has('start_date'))
        <span class="help-block">
            <strong>{{ $errors->first('start_date') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
<label for="end_date">Час завершення тестування</label>
<div class="block{{ $errors->has('end_date') ? ' has-error' : '' }}">
    <input id="end_date" class="datetimepicker form-control" name="end_date" type="text" value="{{ (old('end_date') ? old('end_date') : ((isset($test)) ? $test->end_date : '' )) }}"/>
    @if ($errors->has('end_date'))
        <span class="help-block">
            <strong>{{ $errors->first('end_date') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
    <label for="bonus">
        <input id="bonus" type="checkbox" name="bonus" {{ ((isset($test) && isset($test->bonus)) ? 'checked' : '' ) }}>
        Бонус першим, хто пройде тест
    </label>
</div>

<div class="form-group">
<label for="bonus_points">Бали за перше проходження</label>
<div class="block{{ $errors->has('bonus_points') ? ' has-error' : '' }}">
    <input id="bonus_points" class="form-control" name="bonus_points" type="number" value="{{ (old('bonus_points') ? old('bonus_points') : ((isset($test) && isset($test->bonus)) ? $test->bonus->points : '0' )) }}"/>
    @if ($errors->has('bonus_points'))
        <span class="help-block">
            <strong>{{ $errors->first('bonus_points') }}</strong>
        </span>
    @endif
</div>
</div>

<div class="form-group">
<label for="bonus_count">Кількість перших проходжень</label>
<div class="block{{ $errors->has('bonus_count') ? ' has-error' : '' }}">
    <input id="bonus_count" class="form-control" name="bonus_count" type="number" value="{{ (old('bonus_count') ? old('bonus_count') : ((isset($test) && isset($test->bonus)) ? $test->bonus->bonus_count : '0' )) }}"/>
    @if ($errors->has('bonus_count'))
        <span class="help-block">
            <strong>{{ $errors->first('bonus_count') }}</strong>
        </span>
    @endif
</div>
</div>