@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <h4 class="pull-left">Рейтинг</h4>
        </div>
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]) . '#display_user_fields' }}" class="btn btn-xs pull-left">
                <i class="fa fa-cog"></i>
                Налаштування відобрашення таблиці
            </a>
            <table class="admin_table">
                <thead>
                <tr>
                    @foreach($faculty->displayRatingInformationFields() as $information)
                        <th>{{ $information->name }}</th>
                    @endforeach
                    <th><i class="fa fa-fw fa-star"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr data-href="{{ route('admin.faculties.faculty.rating.user', ['faculty' => $faculty->slug, 'user_id' => $user->id]) }}">
                        @foreach($faculty->displayRatingInformationFields() as $information)
                            <td>{{ $user->getInformation($information->slug) }}</td>
                        @endforeach
                        <td>{{ $user->rating }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="col-md-12 text-center">
                {{ $users->links() }}
            </div>
            <script>
                $(document).ready(function() {
                    $('[data-href]').click(function (e) {
                        window.location.href = this.dataset.href;
                    });
                });
            </script>
        </div>
    </div>
@endsection