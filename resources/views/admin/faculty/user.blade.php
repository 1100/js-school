@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.users', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
                <i class="fa fa-arrow-left"></i>
                Всі студенти
            </a>
            <h4 class="pull-left">Редагування студента</h4>
        </div>
        <form role="form" method="POST" action="{{ route('admin.faculties.faculty.edit_user', ['faculty' => $faculty->slug, 'user_id' => $user->id]) }}" class="col-md-12">
            {{ csrf_field() }}

            <input type="hidden" name="faculty" value="{{ $faculty->slug }}">
            <input type="hidden" name="id" value="{{ $user->id }}">

            @foreach($faculty->information()->where('slug', '!=', 'facebook')->get() as $field)
            @php ($field->value = $user->getInformation($field->slug))
            @include('user_information.input.'.$field->type)
            @endforeach

            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                <label for="status">Статус</label>
                <select id="status" class="form-control" name="status" required>
                    @foreach ($statuses as $status)
                        <option @if($user->status == $status->slug) selected @endif value="{{ $status->slug }}">{{ $status->name }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Зберегти</button>
        </form>
    </div>
@endsection