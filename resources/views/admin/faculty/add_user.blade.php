@extends('admin.layouts.faculty')
@section('faculty_content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('admin.faculties.faculty.users', ['faculty' => $faculty->slug]) }}" class="btn btn-warning pull-right">
                <i class="fa fa-arrow-left"></i>
                Всі студенти
            </a>
            <h4 class="pull-left">Новий студент</h4>
        </div>
        <form role="form" method="POST" action="{{ route('admin.faculties.faculty.add_user', ['faculty' => $faculty->slug]) }}" class="col-md-12">
            {{ csrf_field() }}

            <input type="hidden" name="faculty" value="{{ $faculty->slug }}">

            @foreach($faculty->information()->where('slug', '!=', 'facebook')->get() as $field)
            @include('user_information.input.'.$field->type)
            @endforeach

            <button type="submit" class="btn btn-primary">Додати</button>
        </form>
    </div>
@endsection