@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @can('permission', 'edit-user-information')
                <h4>Редагування поля інформації</h4>
                <form role="form" method="POST" action="{{ route('admin.edit_user_information', ['user_information_slug' => $field->slug]) }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('field_name') ? ' has-error' : '' }}">
                        <label for="field_name" class="control-label">Назва:</label>
                        <input id="field_name" type="text" class="form-control" name="field_name" value="{{ $field->name }}" required autofocus>
                        @if ($errors->has('field_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('field_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('field_description') ? ' has-error' : '' }}">
                        <label for="field_description" class="control-label">Опис:</label>
                        <input id="field_description" type="text" class="form-control" name="field_description" value="{{ $field->description }}">
                        @if ($errors->has('field_description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('field_description') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type" class="control-label">Тип:</label>
                        <select id="type" class="form-control custom-select" name="type" value="{{ $field->type }}" required>
                        @foreach ($information_types as $type)
                            <option value="{{ $type->slug }}" @if($field->type == $type->slug) selected @endif>
                                {{ $type->name }}
                            </option>
                        @endforeach
                        </select>
                        @if ($errors->has('type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('field_login') ? ' has-error' : '' }}">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="field_login" value="1" @if($field->login) checked @endif>
                                Використовувати як логін
                            </label>
                        </div>

                        @if ($errors->has('field_login'))
                            <span class="help-block">
                                <strong>{{ $errors->first('field_login') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            Зберегти
                        </button>
                    </div>
                </form>
                @endcan
            </div>
        </div>
    </div>

@endsection