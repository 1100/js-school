@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <table class="admin_table">
                    <caption><h4>Всі викладачі</h4></caption>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Логін</th>
                        <th>Групи</th>
                        <th>Ролі</th>
                        <th>Дії</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $admin)
                        <tr>
                            <td> {{ $admin->id }} </td>
                            <td> {{ $admin->login }} </td>
                            <td>
                                @foreach($admin->faculties as $faculty)
                                    {{ $loop->first ? '' : ', ' }}
                                    {{ $faculty->name }}
                                @endforeach
                            </td>
                            <td>
                                @foreach($admin->roles as $role)
                                    {{ $loop->first ? '' : ', ' }}
                                    {{ $role->name }}
                                @endforeach
                            </td>
                            <td>
                                @can('permission', 'edit-admin')
                                <a class="text-info" href="{{ route('admin.show_admin', ['admin_id' => $admin->id]) }}">
                                    <button>
                                    <i class="fa fa-pencil"></i> редагувати
                                    </button>
                                </a>
                                @endcan
                                @can('permission', 'delete-admin')
                                <form method="post" class="form-inline" action="{{ route('admin.delete_admin', ['admin_id' => $admin->id]) }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                <button type="submit" class="text-danger">
                                    <i class="fa fa-times"></i> видалити
                                </button>
                                </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <br>
                @can('permission', 'add-admin')
                <form role="form" method="POST" action="{{ url('admin/create_admin') }}">
                    {{ csrf_field() }}
                    <h4>Додати викладача</h4>
                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                        <label class="sr-only" for="login">Логін</label>
                        <input type="text" class="form-control" id="login" name="login" placeholder="Логин">
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label class="sr-only" for="password">Пароль</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
                    </div>
                    <div class="form-group">
                        <b>Групи:</b>
                        @foreach($faculties as $faculty)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="faculties[]" value="{{ $faculty->slug }}">
                                    {{ $faculty->name }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <b>Ролі:</b>
                    @foreach($roles as $role)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="roles[]" value="{{ $role->slug }}">
                                {{ $role->name }}
                            </label>
                        </div>
                    @endforeach
                    </div>
                    <button type="submit" class="btn btn-primary">Додати</button>
                </form>
                @endcan
            </div>
        </div>
    </div>

@endsection