@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row panel-group">
            <div class="col-md-10 col-md-offset-1 row">

                <ol class="breadcrumb">
                    <li><a href="{{ route('admin.faculties.show') }}">Групи</a></li>
                    <li><a href="{{ route('admin.faculties.faculty.show', ['faculty' => $faculty->slug]) }}">{{ $faculty->name }}</a>
                    @if(request()->route()->getName() == 'admin.faculties.faculty.show_user'
                        || request()->route()->getName() == 'admin.faculties.faculty.add_user_form')
                    <li><a href="{{ route('admin.faculties.faculty.users', ['faculty' => $faculty->slug]) }}"> Студенти </a></li>
                    @endif
                    <li class="active">
                    @if (request()->route()->getName() == 'admin.faculties.faculty.users')
                        Студенти
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.rating.all')
                        Рейтинг
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.user_information')
                        Інформація студентів
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.test.all')
                        Тести
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.curators')
                        Викладачі
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.settings.show')
                        Налаштування
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.show_user')
                        Редагування студента
                    @elseif (request()->route()->getName() == 'admin.faculties.faculty.add_user_form')
                        Новий студент
                    @endif
                    </li>
                </ol>

                <div class="row panel panel-body">
                    <div class="col-md-4">
                        <h4>{{ $faculty->name }}</h4>
                        <ul class="nav nav-pills nav-stacked">
                            @can('permission', 'users')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.users'
                            || request()->route()->getName() == 'admin.faculties.faculty.add_user_form'
                            || request()->route()->getName() == 'admin.faculties.faculty.show_user') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.users', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-users"></i>
                                    Студенти
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'rating')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.rating.all') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.rating.all', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-star"></i>
                                    Рейтинг
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'faculty-user-information')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.user_information') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.user_information', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-id-card-o"></i>
                                    Інформація студентів
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'tasks')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.test.all'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.add'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.edit'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.question.all'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.question.edit'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.question.add'
                            || request()->route()->getName() == 'admin.faculties.faculty.test.add') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.test.all', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-check-square-o"></i>
                                    Тести
                                </a>
                            </li>
                            @endcan
                            @can('permission', 'admins')
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.curators') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.curators', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-user-secret"></i>
                                    Викладачі
                                </a>
                            </li>
                            @endcan
                            @if( Gate::allows('permission', 'faculty-settings') || Gate::allows('permission', 'faculty-display-settings') )
                            <li @if (request()->route()->getName() == 'admin.faculties.faculty.settings.show') class="active" @endif>
                                <a href="{{ route('admin.faculties.faculty.settings.show', ['faculty' => $faculty->slug]) }}">
                                    <i class="fa fa-fw fa-cogs"></i>
                                    Налаштування
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-md-8 faculty-content">
                        @yield('faculty_content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection