@extends('admin.layouts.default')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form role="form" method="POST" action="{{ url('admin/admin/'.$admin->id) }}">
                    {{ csrf_field() }}
                    <h4>Редагування викладача {{ $admin->login }}</h4>
                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                        <label for="login">Логин</label>
                        <input type="text" class="form-control" id="login" name="login" placeholder="Логин" value="{{ $admin->login }}">
                    </div>
                    <div class="form-group">
                        <b>Групи:</b>
                        @foreach($faculties as $faculty)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="faculties[]" @if($admin->faculties->contains($faculty)) checked @endif value="{{ $faculty->slug }}">
                                    {{ $faculty->name }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <b>Ролі:</b>
                        @foreach($roles as $role)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="roles[]" @if($admin->roles->contains($role)) checked @endif value="{{ $role->slug }}">
                                    {{ $role->name }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <a class="btn btn-danger" href="{{ url('admin/all_admins') }}">Відмінити</a>
                    <button type="submit" class="btn btn-primary">Зберегти</button>
                </form>

            </div>
        </div>
    </div>

@endsection