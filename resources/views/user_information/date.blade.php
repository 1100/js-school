<label for="{{ $field->slug }}">{{ $field->name }}</label>
<div class="block">
    <b id="{{ $field->slug }}">{{ $field->value }}</b>
</div>