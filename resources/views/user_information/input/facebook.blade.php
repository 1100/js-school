@if (Auth::user())
    @if (!Auth::user()->getInformation('facebook')) {{-- Привязать фейсбук --}}
        <div class="block">
            Ваш Facebook:
            нет
            <a href="{{ url('/login/facebook') }}" class="more blue"><i class="fa fa-facebook"></i> Привязать Facebook</a>
        </div>
    @else {{-- Отвязать фейсбук --}}
        <div class="block">
            Ваш Facebook:
            <a href="https://fb.com/app_scoped_user_id/{{ Auth::user()->getInformation('facebook') }}" target="_blank">{{  Auth::user()->getInformation('facebook') }}</a>
            <a href="{{ url('cabinet/delete_facebook') }}" class="more blue">
                Отвязать Facebook
            </a>
        </div>
    @endif
@else
    <div class="block">
        <a href="{{ route('login.facebook', ['faculty' => (isset($faculty->slug))?$faculty->slug:null]) }}" class="more blue"><i class="fa fa-facebook"></i> Войти через Facebook</a><br>
    </div>
@endif