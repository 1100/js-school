<div class="form-group{{ $errors->has($field->slug) ? ' has-error' : '' }}">
    <label for="{{ $field->slug }}" class="control-label">{{ $field->name }}</label>

    <input type="hidden" name="{{ $field->slug }}" value="0"/>
    <input id="{{ $field->slug }}" type="checkbox" class="form-control" name="{{ $field->slug }}" @if( (isset($field->value) && !empty($field->value)) || (isset($field->value) && $field->value == '1')) checked @endif value="1">

    @if ($errors->has($field->slug))
        <span class="help-block">
            <strong>{{ $errors->first($field->slug) }}</strong>
        </span>
    @endif
</div>