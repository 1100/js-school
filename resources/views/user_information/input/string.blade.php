<label for="{{ $field->slug }}">{{ $field->name }}</label>
<div class="block{{ $errors->has($field->slug) ? ' has-error' : '' }}">
    <input id="{{ $field->slug }}" class="text_input form-control" name="{{ $field->slug }}" type="text" value="{{ $field->value or old($field->slug) }}" />
    @if ($errors->has($field->slug))
        <span class="help-block">
            <strong>{{ $errors->first($field->slug) }}</strong>
        </span>
    @endif
</div>