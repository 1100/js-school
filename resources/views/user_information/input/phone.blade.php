<label for="{{ $field->slug }}">{{ $field->name }}</label>
<div class="block{{ $errors->has($field->slug) ? ' has-error' : '' }}">
    <input id="{{ $field->slug }}" class="text_input" name="{{ $field->slug }}" type="text" value="{{ $field->value or old($field->slug) }}" />
    @if ($errors->has($field->slug))
        <span class="help-block">
            <strong>{{ $errors->first($field->slug) }}</strong>
        </span>
    @endif
</div>
<script src="{{asset('js/jquery.maskedinput.min.js')}}"></script>
<script>
    jQuery(function($) {
        var phone_input = $('#phone');
        phone_input.attr('placeholder', '+38 (0__) ___-__-__');
        phone_input.mask('+38 (099) 999-99-99');
    });
</script>