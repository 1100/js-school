<label for="{{ $field->slug }}">{{ $field->name }}</label>
<div class="block">
    <a href="{{ $field->value }}" target="_blank"><b id="{{ $field->slug }}">{{ $field->value }}</b></a>
</div>