@extends('layouts.with_sidebar')

@section('content')
    <div class="page relative noborder">
        <div class="slider_content_box small_hero clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Особистий кабінет
                </h1>
            </div>
        </div>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                @include('cabinet.dashboard-alerts')

                @if ($user->status === 'approved')

                    @if ($faculty->isEnabledInvite())
                        <div class="columns clearfix margin_top_10">
                            <h3 class="box_header">
                                Приглашение друзей
                            </h3>
                            <form class="{{ $errors->has('email') ? ' has-error' : '' }} form col-md-12" role="form" method="POST" action="{{ route('cabinet.send_invite') }}">
                                {{ csrf_field() }}

                                <label for="email">Email</label>
                                <div class="block{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" class="text_input form-control" name="email" type="email" value="{{ old('email') }}" />
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <button type="submit" class="more blue">Пригласить</button>

                            </form>
                        </div>
                    @endif

                    <div class="columns clearfix margin_top_10">
                        <h3 class="box_header">
                            Детальний рейтинг
                        </h3>

                        @if($user_ratings)
                            <table class="rating_table">
                                <thead>
                                <tr>
                                    <th>Тест</th>
                                    <th>Бали</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user_ratings as $point)
                                    <tr>
                                        <td>{{ $point['name'] }}</td>
                                        <td>{{ $point['points'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>
                @endif

            </div>
@endsection
@section('sidebar')
    @include('layouts.partials.sidebar_cabinet')
        </div>
    </div>
@endsection