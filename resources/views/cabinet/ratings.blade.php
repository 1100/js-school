@extends('layouts.with_sidebar')

@section('content')
    <div class="page relative noborder">
        <div class="slider_content_box small_hero clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Особистий кабінет
                </h1>
            </div>
        </div>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Рейтинги
                </h3>
                <div class="columns clearfix margin_top_10">
                    @if ($user->status !== 'approved')
                        @include('cabinet.dashboard-not-approved-alerts')
                    @else
                        <table class="rating_table">
                            <thead>
                            <tr>
                                @foreach($faculty->displayRatingInformationFields() as $information)
                                    <th>{{ $information->name }}</th>
                                @endforeach
                                <th class="points_col"><i class="fa fa-fw fa-star"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rating_users as $rating_user)
                                <tr>
                                    @foreach($faculty->displayRatingInformationFields() as $information)
                                        <td>{{ $rating_user->getInformation($information->slug) }}</td>
                                    @endforeach
                                    <td class="points_col">{{ $rating_user->rating }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center" style="width: 100%;">
                            {{ $rating_users->links() }}
                        </div>
                    @endif
                </div>
            </div>
            @endsection
            @section('sidebar')
                @include('layouts.partials.sidebar_cabinet')
        </div>
    </div>
@endsection