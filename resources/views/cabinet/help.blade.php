@extends('layouts.with_sidebar')

@section('content')
    <div class="page relative noborder">
        <div class="slider_content_box small_hero clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Особистий кабінет
                </h1>
            </div>
        </div>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Допомога
                </h3>
                <div class="columns clearfix">
                    <p class="text">
                        Виникли запитання? Ми завжди раді відповісти.
                        Відправте Ваше повідомлення через контактну форму — ми обов'язково відповімо!
                    </p>
                    <form class="form" method="POST">
                        {{ csrf_field() }}
                        <fieldset style="clear:both;">
                            <label>Ваше повідомлення</label>
                            <div class="block{{ $errors->has('message') ? ' has-error' : '' }}">
                                <textarea name="message"></textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                                @endif
                            </div>
                            <button type="submit" class="more blue">Відправити</button>
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </fieldset>
                    </form>
                </div>
            </div>
@endsection
@section('sidebar')
    @include('layouts.partials.sidebar_cabinet')
        </div>
    </div>
@endsection