@extends('layouts.with_sidebar')

@section('content')
    <div class="page relative noborder">
        <div class="slider_content_box small_hero clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Особистий кабінет
                </h1>
            </div>
        </div>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                @if($errors->any())
                    <div id="information-errors" class="alert alert-danger alert-dismissable error">
                        <span class="close" data-hide="information-errors" aria-label="close">&times;</span>
                        <b>Помилка</b>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h3 class="box_header">
                    Основна інформація
                </h3>
                <div class="columns clearfix">
                    <div class="form">

                        @foreach($login_fields as $field)
                            @php ($field->value = $user->getInformation($field->slug))
                            @if($field->value && $field->slug != 'facebook')
                                @include('user_information.'.$field->type)
                            @else
                                @include('user_information.'.$field->type)
                            @endif
                        @endforeach

                    </div>
                </div>
                <h3 class="box_header">
                    Фотографія
                </h3>
                <div class="columns clearfix">
                    <form class="form" method="POST" enctype="multipart/form-data" action="{{ route('cabinet.information.change_avatar') }}">
                        {{ csrf_field() }}
                        <div class="block change_avatar">
                            <div class="avatar_wrapper" style="background-image: url('{{ $user->avatarUrl() }}')">
                            </div>
                            <label class="file_wrapper">
                                <button type="submit" class="more blue">Завантажити фотографію</button>
                                <input type="file" id="avatar" name="avatar" placeholder="Зображення">
                            </label>
                            <div class="save_wrapper">
                                <button type="submit" class="more blue" disabled="true" id="save_avatar_btn">Сохранить</button>
                            </div>
                        </div>
                        <button type="submit" class="more blue">Зберегти</button>
                    </form>
                    <script>
                        function handleImagesSelect(evt) {
                            var files = evt.target.files;

                            for (var i = 0, f; f = files[i]; i++) {
                                var reader = new FileReader();
                                reader.onload = (function(image) {
                                    return function(e) {
                                        var avatar_wrapper = document.createElement('div');
                                        avatar_wrapper.className = 'avatar_wrapper';
                                        avatar_wrapper.innerHTML = '<div class="avatar_wrapper" style="background-image: url('+e.target.result +');"></div> ';
                                        var old_avatar_wrapper = document.querySelectorAll('.change_avatar .avatar_wrapper')[0];
                                        old_avatar_wrapper.parentNode.replaceChild(avatar_wrapper, old_avatar_wrapper);
                                        document.getElementById('save_avatar_btn').disabled = false;
                                    };
                                })(f);
                                reader.readAsDataURL(f);
                            }
                        }

                        document.getElementById('avatar').addEventListener('change', handleImagesSelect, false);
                    </script>
                </div>
                @if($additional_fields->isNotEmpty())
                <h3 class="box_header">
                    Додаткова інформація
                </h3>
                <div class="columns clearfix">
                    <form class="form" method="POST" action="{{ route('cabinet.information.change_additional') }}">
                        {{ csrf_field() }}
                        @foreach($additional_fields as $field)
                            @php ($field->value = $user->getInformation($field->slug))
                            @include('user_information.input.'.$field->type)
                        @endforeach
                        <button type="submit" class="more blue">Зберегти</button>
                    </form>
                </div>
                @endif

                <h3 class="box_header">
                    Зміна паролю
                </h3>
                <div class="columns clearfix">
                    <form class="form" method="POST" action="{{ route('cabinet.information.change_password') }}">
                        {{ csrf_field() }}
                        <label>Новий пароль</label>
                        <div class="block">
                            <input class="text_input" name="password" type="password" />
                        </div>
                        <label>Повтор паролю</label>
                        <div class="block">
                            <input class="text_input" name="password_confirmation" type="password" />
                        </div>
                        <button type="submit" class="more blue">Зберегти</button>
                    </form>
                </div>
            </div>
@endsection
@section('sidebar')
    @include('layouts.partials.sidebar_cabinet')
        </div>
    </div>
@endsection