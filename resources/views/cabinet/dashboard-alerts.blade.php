@if ($user->status !== 'approved')
    @include('cabinet.dashboard-not-approved-alerts')
@else
    @foreach($tests as $test)
        @if ($test->getStatus() == 'open')
            @if ($test->getPassingStatus($user->id) == 'not_passing')
                    <div class="alert alert-success">
                        <span>
                            Тест "{{ $test->name }}" доступний для проходження.<br>
                            <a href="{{ route('cabinet.tests') }}">Перейти до списку тестів</a>.
                        </span>
                    </div>
            @elseif($test->getPassingStatus($user->id) == 'passing')
                <div class="alert alert-danger">
                    <span>
                        Внимание! Сейчас идёт тестирование по теме "{{ $test->name }}".<br>
                        После окончания времени тестирования у Вас не будет возможности пересдать тест!<br>
                        <a href="{{ route('cabinet.test.show', ['test_id' => $test->id]) }}">Перейти к тесту</a>.
                    </span>
                </div>
            @endif
        @endif
    @endforeach
@endif