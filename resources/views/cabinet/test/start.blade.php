@extends('layouts.with_sidebar')

@section('content')
    <div class="page relative noborder">
        <div class="slider_content_box small_hero clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Особистий кабінет
                </h1>
            </div>
        </div>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Тестування на тему: {{ $test->name }}
                </h3>
                <div class="columns clearfix margin_top_10">
                    <div class="test_meta">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        {{ date('d.m', strtotime($test->start_date)) }}
                        -
                        {{ date('d.m', strtotime($test->end_date)) }}
                    </div>
                    <div id="oneTestHeader">
                        <span id="timeLeft"></span>
                        <h3 id="questionsLeft"></h3>
                    </div>
                    <div id="testBegin">
                        {{ $test->description }}
                        <h4><br>Увага!</h4>
                        <p>
                            Після початку тестування у Вас не буде можливості перездати тест або зробити паузу.<br><br>
                            Вам потрібно відповісти на<b>{{ $test->questions_count() }}</b> запитань.<br>
                            На проходження тесту відводиться <b>{{ $test->duration }}</b> хвилин.<br><br>
                            Для початку тестування натисніть кнопку "Розпочати тест"
                        </p>
                        <p>
                            <a href="{{ route('cabinet.tests') }}">Повернутись до списку тестів</a>
                        </p>
                        <button id="startTest" class="more blue" onclick="startTest({{ $test->id }});">{{ ($test->getPassingStatus() == 'passing') ? 'Продовжити тест' : 'Розпочати тест' }}</button>
                    </div>
                    <div id="testBody">
                    </div>
                </div>

                <script>
                    var subjects = [];
                    var question_counts = [];
                    var durations = [];
                    subjects[{{ $test->id }}] = '{{ $test->name }}';
                    question_counts[{{ $test->id }}] = '{{ $test->questions_count() }}';
                    durations[{{ $test->id }}] = '{{ $test->duration }}';
                </script>
                <script type="text/javascript" src="{{ asset('js/tests.js') }}"></script>
            </div>
            @endsection
            @section('sidebar')
                @include('layouts.partials.sidebar_cabinet')
        </div>
    </div>
@endsection