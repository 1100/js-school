@extends('layouts.with_sidebar')

@section('content')
    <div class="page relative noborder">
        <div class="slider_content_box small_hero clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Особистий кабінет
                </h1>
            </div>
        </div>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Тестування по темі: {{ $test->name }}
                </h3>
                <div class="columns clearfix margin_top_10">
                    <div class="test_meta">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        {{ date('d.m', strtotime($test->start_date)) }}
                        -
                        {{ date('d.m', strtotime($test->end_date)) }}
                    </div>
                    <div id="oneTestHeader">
                        <span id="timeLeft"></span>
                        <h3 id="questionsLeft"></h3>
                    </div>
                    <p>{{ $test->description }}</p>
                    Тест завершено.<br>
                    Правильних відповідей: <b>{{ $test_session->correctAnswersCount() }}</b> з <b>{{ $test_session->test()->first()->questions_count() }}</b>.<br>
                    За тест нараховано <b>{{ $test_session->points->number() }}</b> балів.
                    <br>
                    <p><a href="{{ route('cabinet.tests') }}">Повернктись до списку тестів</a></p>
                </div>
            </div>
            @endsection
            @section('sidebar')
                @include('layouts.partials.sidebar_cabinet')
        </div>
    </div>
@endsection