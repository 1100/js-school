@extends('layouts.with_sidebar')

@section('content')
    <div class="page relative noborder">
        <div class="slider_content_box small_hero clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Особистий кабінет
                </h1>
            </div>
        </div>
        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Тестування
                </h3>
                <div class="columns clearfix margin_top_10" id="allTests">
                    @if ($user->status !== 'approved')
                        @include('cabinet.dashboard-not-approved-alerts')
                    @else
                    @foreach($tests as $i => $test)
                        <div class="test_block {{ $test->getStatus() == 'open' && $test->getPassingStatus($user->id) == 'not_passing' ? 'active' : '' }} margin_top_10" data-href="{{ route('cabinet.test.show', ['test_id' => $test->id]) }}">
                            <div class="test_status">
                                @if($test->getPassingStatus($user->id) == 'completed')
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                @elseif($test->getPassingStatus($user->id) == 'passing')
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                @else
                                <i class="fa fa-square-o" aria-hidden="true"></i>
                                @endif
                            </div>
                            <div class="test_content">
                                <h4 class="test_title">Тема {{ $i+1 }}. {{ $test->name }}</h4>
                                <div class="test_meta">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                    {{ date('d.m', strtotime($test->start_date)) }}
                                    -
                                    {{ date('d.m', strtotime($test->end_date)) }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endif
                </div>

            </div>
@endsection
@section('sidebar')
    @include('layouts.partials.sidebar_cabinet')
        </div>
    </div>
@endsection