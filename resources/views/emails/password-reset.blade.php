<font face="Arial, sans-serif" size="2" color="#000">
    <h1 style="margin-top:25px;"><font size="+1" color="#3a75c4">Сброс пароля</font></h1>
    <p>Вы получили это письмо так как запросили сброс пароля на своём аккаунте JS-school.</p>
    <p><a href="{{ route('password.reset',['token'=>$token]) }}">Восстановить пароль</a></p>
    <p>Если вы не запрашивали сброс пароля, проигнорируйте данное сообщение.</p>
    <p style="font-style:italic;">С пожеланиями успехов и вдохновения в обучении, <br> команда JS-school.</p>
    <p><a href="{{ url('/') }}">{{ url('/') }}</a>
</font>