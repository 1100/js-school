<font face="Arial, sans-serif" size="2" color="#000">
    <h1 style="margin-top:25px;"><font size="+1" color="#3a75c4">Приглашаем вас в JS-школу</font></h1>
    <p>Один из Ваших друзей пригласил Вас к обучению в JS-school.</p>
    <p>Для того, чтобы принять участие, подайте заявку на регистрацию по этой ссылке:</p>
    <p><a href="{{ route('invite', ['faculty' => $faculty, 'invited_key' => $invite_key]) }}">{{ route('invite', ['faculty' => $faculty, 'invited_key' => $invite_key]) }}</a></p>
    <p style="font-style:italic;">С пожеланиями успехов и вдохновения в обучении, <br> команда JS-school.</p>
    <p><a href="{{ url('/') }}">{{ url('/') }}</a>
</font>