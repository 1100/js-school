<font face="Arial, sans-serif" size="2" color="#000">
    <h1 style="margin-top:25px;"><font size="+1" color="#3a75c4">Приглашаем Вас в JS-school</font></h1>
    <p>Для того, чтобы принять участие, сбросьте пароль для своего аккаунта по этой ссылке:</p>
    <p><a href="{{ route('password.reset.from_email',['email'=>$user_email]) }}">Сбросить пароль</a></p>
    <p style="font-style:italic;">С пожеланиями успехов и вдохновения в обучении, <br> команда JS-school.</p>
    <p><a href="{{ url('/') }}">{{ url('/') }}</a>
</font>