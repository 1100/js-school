@extends('layouts.default')
@section('slider')
@endsection
@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula maximus ullamcorper. Suspendisse potenti. Sed varius justo ante, vitae tempor magna hendrerit nec. Integer mollis libero pretium turpis eleifend auctor. Etiam mattis auctor tortor vitae elementum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In imperdiet nisl eget est volutpat, non vulputate nulla tincidunt. Nulla feugiat consequat urna vitae dictum. Nulla bibendum nisi turpis, ac vestibulum turpis vestibulum ac. Maecenas varius ultricies felis, id mollis ex.
            </p>
            <img class="image" src="/images/samples/image_02.jpg">
            <p>
                Donec ac metus sit amet ex tristique imperdiet. In non arcu dapibus, suscipit est et, cursus mauris. Duis ornare imperdiet quam eu feugiat. Nulla tempor ipsum at augue venenatis, aliquam tristique nulla sodales. Sed commodo vel elit at vestibulum. Ut vitae tempus magna, id venenatis turpis. Sed finibus rutrum finibus. Morbi sollicitudin massa sit amet ex venenatis finibus. Curabitur at porta urna.
            </p>
            <p>
                Proin sollicitudin, elit sagittis blandit interdum, elit purus dignissim odio, eu pulvinar augue orci ut sem. Quisque rutrum rhoncus est quis ullamcorper. Duis ac ultricies nibh. Cras vehicula dui quis nunc porta aliquam. Phasellus sit amet varius urna, ac hendrerit dui. Donec quis eleifend orci. Duis cursus libero non est lobortis, at posuere sapien auctor. Donec sodales porta magna ullamcorper ullamcorper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus.
            </p>

        </div>
    </div>
@endsection