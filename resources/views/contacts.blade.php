@extends('layouts.default')
@section('slider')
@endsection
@section('content')
    <div class="page relative noborder">
        <div class="page_layout page_margin_top clearfix">
            <h3 class="box_header">
                Контакти
            </h3>
            <div class="page_left">
                <p class="text">
                    Виникли запитання? Ми завжди раді відповісти. Відправте Ваше повідомлення через контактну форму — ми обов'язково відповімо!
                </p>
                <form class="form" method="post">
                    {{ csrf_field() }}
                    <fieldset class="left">
                        <label>Ім'я</label>
                        <div class="block{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <input class="text_input" name="first_name" type="text" value="{{ old('first_name') }}"/>
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </fieldset>
                    <fieldset class="right">
                        <label>Прізвище</label>
                        <div class="block{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <input class="text_input" type="text" name="last_name" value="{{ old('last_name') }}"/>
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </fieldset>
                    <p class="text-info">
                        Заповніть хоча б одне поле контактних даних, щоб ми могли з Вами звязатись:
                    </p>
                    <fieldset class="left">
                        <label><sup class="text-danger">*</sup> Телефон</label>
                        <div class="block{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input class="text_input" name="phone" type="text" value="{{ old('phone') }}"/>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </fieldset>
                    <fieldset class="right">
                        <label><sup class="text-danger">*</sup> E-mail</label>
                        <div class="block{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input class="text_input" type="text" name="email" value="{{ old('email') }}"/>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </fieldset>
                    <fieldset style="clear:both;">
                        <label><sup class="text-danger">*</sup> Ваше повідомлення</label>
                        <div class="block{{ $errors->has('message') ? ' has-error' : '' }}">
                            <textarea name="message">{{ old('message') }}</textarea>
                            @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                        </div>
                        <sup class="text-danger">*</sup> обязательные для заполнения поля
                        <button type="submit" class="more blue">Відправити</button>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection