@extends('layouts.default')
@section('slider')
    @include('layouts.partials.home_slider')
@endsection
@section('content')
    <div class="page relative noborder">
        <!-- slider content -->
        <div class="slider_content_box clearfix">
            <div class="slider_content" style="display: block;">
                <h1 class="title">
                    Базовий курс JavaScript
                </h1>
                <h2 class="subtitle">
                    <a href="{{ route('register', 'basic') }}">
                        Запрошуємо зареєструватись
                    </a>
                </h2>
            </div>
            <div class="slider_content">
                <h1 class="title">
                    Поглиблений курс JavaScript
                </h1>
                <h2 class="subtitle">
                    <a href="{{ route('register', 'advanced') }}">
                        Запрошуємо зареєструватись
                    </a>
                </h2>
            </div>
            <div class="slider_content">
                <h1 class="title">
                    Будьте в курсі<br/>
                    останніх новин
                </h1>
                <h2 class="subtitle">
                    Підпишіться на розсилку
                </h2>
            </div>
        </div>
        <!-- home box -->
        <ul class="home_box_container clearfix">
            @foreach($faculties as $faculty)
                <li class="home_box faculty-{{ $faculty->color }} animated_element  {{ ($loop->last ? 'animation-slideRight duration-800 delay-250' : 'animation-fadeIn duration-500 ') }}">
                    <h1>
                        {{ $faculty->name }}
                    </h1>
                    <h3>
                        {{ $faculty->description }}
                    </h3>
                    <div class="news clearfix">
                        <a class="more light medium icon_small_arrow margin_right_white" href="{{ route('register', $faculty->slug) }}" title="Реєстрація">Реєстрація</a>
                    </div>
                </li>
            @endforeach
            <li class="home_box faculty-red animated_element animation-slideRight200 duration-800 delay-500">
                <h2>
                    Підпишись на новини школи
                </h2>
                <ul class="news subscribe_form clearfix">

                    <input class="email_input" id="subscribe-email" name="subscribe-email" type="email" placeholder="Email" />

                        <button id="btn-subscribe" class="more light medium icon_small_arrow margin_right_white">Підписатись</button>
                        <div class="alert-messages">
                            <div id="subscribe-success" class="alert alert-success alert-dismissable success" style="display: none;">
                                <span class="close" data-hide="subscribe-success" aria-label="close">&times;</span>
                                <span id="subscribe-success-message">Ви успішно підписались на новини</span>
                            </div>
                            <div id="subscribe-error" class="alert alert-danger alert-dismissable error" style="display: none;">
                                <span class="close" data-hide="subscribe-error" aria-label="close">&times;</span>
                                <span id="subscribe-error-message">Виникла помилка. Можливо Ви вже підписані на новини або Ваш email введено не вірно</span>
                            </div>
                        </div>


                    <script src="{{asset('js/subscribe.js')}}"></script>
                </ul>
            </li>
        </ul>

        <div class="page_layout page_margin_top clearfix">
            <div class="page_left">
                <h3 class="box_header">
                    Новини школи
                </h3>
                <div class="columns clearfix">
                    <div class="page_left">
                        <ul class="blog clearfix">
                            @foreach($posts as $post)
                                <li class="post">
                                    <ul class="comment_box">
                                        <li class="date clearfix animated_element animation-slideRight">
                                            <div class="value">{{ $post->created_at->format('d.m.y') }}</div>
                                            <div class="arrow_date"></div>
                                        </li>
                                    </ul>
                                    <div class="post_content">
                                        @if($post->image)
                                            <a class="post_image" href="{{ route('post', ['post_id' => $post->id]) }}" title="{{ $post->title }}">
                                                <img src="{{ Storage::url($post->image) }}" alt="{{ $post->title }}" />
                                            </a>
                                        @endif
                                        <h2>
                                            <a href="{{ route('post', ['post_id' => $post->id]) }}" title="{{ $post->title }}">
                                                {{ $post->title }}
                                            </a>
                                        </h2>
                                        {!! $post->content !!}
                                        <a title="Відкрити" href="{{ route('post', ['post_id' => $post->id]) }}" class="more">
                                            Відкрити &rarr;
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <p>
                    <a class="more blue icon_small_arrow margin_right_white" href="{{ route('news') }}">Всі новини</a>
                </p>
            </div>
            @include('layouts.partials.sidebar')
        </div>
    </div>
@endsection