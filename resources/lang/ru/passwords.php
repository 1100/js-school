<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароли должны содержать не менее шести символов и совпадать.',
    'reset' => 'Ваш пароль изменён!',
    'sent' => 'Мы выслали на вашу электронную почту ссылку на восстановление пароля!',
    'sentSMS' => 'Мы выслали на ваш телефон код для восстановления пароля!',
    'token' => 'Токен сброса пароля недействителен',
    'user' => "Такой пользователь не зарегистрирован",

];
