<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformationFieldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InformationTypesSeeder::class);

        DB::table('information_fields')->insert([
            [
                'slug' => 'email',
                'name' => 'Email',
                'description' => 'Email',
                'login' => true,
                'hidden' => false,
                'default_value' => null,
                'type' => 'email'
            ],
            [
                'slug' => 'phone',
                'name' => 'Телефон',
                'description' => 'Номер телефону',
                'login' => true,
                'hidden' => false,
                'default_value' => null,
                'type' => 'phone'
            ],
            [
                'slug' => 'first_name',
                'name' => 'Ім\'я',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'last_name',
                'name' => 'Прізвище',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'father_name',
                'name' => 'По батькові',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
            [
                'slug' => 'city',
                'name' => 'Місто',
                'description' => '',
                'login' => false,
                'hidden' => false,
                'default_value' => null,
                'type' => 'string'
            ],
        ]);
    }
}
