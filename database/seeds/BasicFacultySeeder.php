<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BasicFacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            [
                'slug' => 'basic',
                'name' => 'Базовий',
                'description' => 'Базовий курс JS',
                'color' => 'yellow'
            ]
        ]);

        $this->call(InformationFieldsForBasicFacultySeeder::class);
    }
}
