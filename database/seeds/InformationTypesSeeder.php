<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('information_types')->insert([
            [
                'slug' => 'boolean',
                'name' => 'Чекбокс',
                'validator' => 'boolean'
            ],
            [
                'slug' => 'date',
                'name' => 'Дата',
                'validator' => 'date'
            ],
            [
                'slug' => 'url',
                'name' => 'URL',
                'validator' => 'url'
            ],
            [
                'slug' => 'facebook',
                'name' => 'facebook',
                'validator' => 'numeric'
            ],
            [
                'slug' => 'email',
                'name' => 'Email',
                'validator' => 'email'
            ],
            [
                'slug' => 'phone',
                'name' => 'Телефон',
                'validator' => 'phone'
            ],
//            [
//                'slug' => 'alpha',
//                'name' => 'Только буквы',
//                'validator' => 'alpha'
//            ],
            [
                'slug' => 'numeric',
                'name' => 'Цифри',
                'validator' => 'numeric'
            ],
            [
                'slug' => 'string',
                'name' => 'Текст',
                'validator' => 'string'
            ],
        ]);
    }
}
