<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformationFieldsForAdvancedFacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('faculty_information_field')->insert([
                [
                    'faculty' => 'advanced',
                    'information_field' => 'email',
                    'required' => true,
                    'display' => true,
                    'rating_display' => false,
                    'position' => 1,
                ],
                [
                    'faculty' => 'advanced',
                    'information_field' => 'first_name',
                    'required' => true,
                    'display' => true,
                    'rating_display' => true,
                    'position' => 2,
                ],
                [
                    'faculty' => 'advanced',
                    'information_field' => 'last_name',
                    'required' => true,
                    'display' => true,
                    'rating_display' => true,
                    'position' => 3,
                ],
            ]
        );
    }
}
