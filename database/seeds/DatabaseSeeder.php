<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InformationFieldsSeeder::class);
        $this->call(BasicFacultySeeder::class);
        $this->call(AdvancedFacultySeeder::class);
        $this->call(PermissionSeeder::class);
    }
}
