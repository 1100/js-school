<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdvancedFacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            [
                'slug' => 'advanced',
                'name' => 'Поглиблений',
                'description' => 'Поглиблений курс JS',
                'color' => 'orange'
            ],
        ]);
        $this->call(InformationFieldsForAdvancedFacultySeeder::class);
    }
}
