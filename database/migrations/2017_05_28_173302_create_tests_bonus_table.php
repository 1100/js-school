<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests_bonus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('test_id')->unsigned();
            $table->integer('points')->unsigned()->default(0);
            $table->integer('bonus_count')->unsigned()->default(0);

            $table->foreign('test_id')->references('id')->on('tests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests_bonus');
    }
}
