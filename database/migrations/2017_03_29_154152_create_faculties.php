<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faculties', function (Blueprint $table) {
            $table->string('slug')->primary();
            $table->string('name');
            $table->string('color')->nullable();
            $table->string('description');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->string('faculty');
            $table->foreign('faculty')->references('slug')->on('faculties');
        });

        Schema::create('faculty_information_field', function (Blueprint $table) {
            $table->string('faculty');
            $table->string('information_field');
            $table->boolean('required')->default(false);
            $table->boolean('display')->default(false);
            $table->integer('position')->default(0);
            $table->unique(['faculty', 'information_field']);

            $table->foreign('faculty')->references('slug')->on('faculties')->onDelete('cascade');
            $table->foreign('information_field')->references('slug')->on('information_fields')->onDelete('cascade');
        });

        Schema::create('admin_faculty', function (Blueprint $table) {
            $table->integer('admin_id')->unsigned();
            $table->string('faculty');
            $table->unique(['admin_id', 'faculty']);

            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
            $table->foreign('faculty')->references('slug')->on('faculties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign(['faculty']);
            $table->dropColumn('faculty');
        });
        Schema::dropIfExists('admin_faculty');
        Schema::dropIfExists('faculty_information_field');
        Schema::dropIfExists('faculties');
    }
}
